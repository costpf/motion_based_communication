% ===================================
% FUNCTION u = control_random()
% -----------
% DESCRIPTION:  Choose the control input randomly from the control space.
% OUTPUT:   Control input chosen.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function u = control_random()
numdisc = 5;
choice = linspace(-0.4,0.4,numdisc);

input = zeros(6, length(choice)^3);

l = 1;
for i = 1:numdisc;
    for j = 1:numdisc;
        for k = 1:numdisc;
        input(:,l) = [choice(i); choice(j); choice(k);0;0;0];
        l = l + 1;
        end
    end
end

p = randperm(length(input));
u = input(:, p(1));


end
