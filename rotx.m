% ===================================
% FUNCTION R = rotx(deg)
% -----------
% DESCRIPTION:  Compute rotation matrix in SO(3) around current x axis with the
%               rotation angle of deg degrees.
% INPUT:    deg [deg]
% OUTPUT:   R (3-by-3 matrix in SO(3))
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function R = rotx(deg)
arg = deg*pi/180.0;
R = [1 0 0;
     0 cos(arg) -sin(arg);
     0 sin(arg) cos(arg)];