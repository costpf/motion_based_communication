% ===================================
% FUNCTION R = roty(deg)
% -----------
% DESCRIPTION:  Compute rotation matrix in SO(3) around current y axis with the
%               rotation angle of deg degrees.
% INPUT:    deg [deg]
% OUTPUT:   R (3-by-3 matrix in SO(3))
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function R = roty(deg)
arg = deg*pi/180.0;
R = [cos(arg) 0 sin(arg);
     0 1 0;
     -sin(arg) 0 cos(arg)];