clear;
clc;

numpoints = 50;
Codebook = code_gen(numpoints);
numsim = 20;
maxstep = 30;

Result_active_obs = zeros(3,maxstep);
det_active_obs = zeros(1,maxstep);
Result_active_sta = zeros(3,maxstep);
det_active_sta =  zeros(1, maxstep);


%Active Policy (eta)
for i = 1:3;
    for j = 1:numsim;
        %True class;
        true_class = i;

        % True initial position
        Rot = rotx(randi(360))*roty(randi(360))*rotz(randi(360));
        theta = acos((trace(Rot) - 1)/2);
        omega = theta/(2*sin(theta)) * [Rot(3,2) - Rot(2,3); Rot(1,3) - Rot(3,1); Rot(2,1) - Rot(1,2)];
        true_r = [omega(1); omega(2); omega(3); 0; 0; 10]; % Column vector!
        
        % Initial trajectory
        zeta = traj_gen(Codebook,true_class);
        eta = proj(true_r,zeta);
        
        % Prior
        [mu, Sigma, log_phi] = prior_rob(eta, Codebook);
        disp(retrieve(log_phi));
        [~, est_z] = max(log_phi);
        if true_class == est_z;
            Result_active_obs(i,1) = Result_active_obs(i,1) + 1/numsim;
        end
        det_active_obs(1) = mean([det(Sigma{1}), det(Sigma{2}), det(Sigma{3})]);
        
        % Display Initial Estimate
        trj_fit(mu,Codebook,zeta,eta,true_r);
        pause(1);
        
        % Update
        for k = 1:maxstep-1;
            [true_r, mu, Sigma, log_phi, true_zeta, eta, u] = active_sim_joint(log_phi, mu, Sigma, Codebook, numpoints, true_r, true_class);
            [~, est_z] = max(log_phi);
            if true_class == est_z;
                Result_active_obs(i,k+1) = Result_active_obs(i,k+1) + 1/numsim;
            end
            det_active_obs(k+1) = mean([det(Sigma{1}), det(Sigma{2}), det(Sigma{3})]);
        end
    end
end

%{
%Active Policy (rz)
for i = 1:3;
    for j = 1:numsim;
        %True class;
        true_class = i;

        % True initial position
        Rot = rotx(randi(360))*roty(randi(360))*rotz(randi(360));
        theta = acos((trace(Rot) - 1)/2);
        omega = theta/(2*sin(theta)) * [Rot(3,2) - Rot(2,3); Rot(1,3) - Rot(3,1); Rot(2,1) - Rot(1,2)];
        true_r = [omega(1); omega(2); omega(3); 0; 0; 10]; % Column vector!
        
        % Initial trajectory
        zeta = traj_gen(Codebook,true_class);
        eta = proj(true_r,zeta);
        
        % Prior
        [mu, Sigma, log_phi] = prior_rob(eta, Codebook);
        disp(retrieve(log_phi));
        [~, est_z] = max(log_phi);
        if true_class == est_z;
            Result_active_sta(i,1) = Result_active_sta(i,1) + 1/numsim;
        end
        det_active_sta(1) = mean([det(Sigma{1}), det(Sigma{2}), det(Sigma{3})]);
        
        % Display Initial Estimate
        trj_fit(mu,Codebook,zeta,eta,true_r);
        pause(1);
        
        % Update
        for k = 1:maxstep-1;
            [true_r, mu, Sigma, log_phi, true_zeta, eta, u] = active_sim_rz(log_phi, mu, Sigma, Codebook, numpoints, true_r, true_class);
            [~, est_z] = max(log_phi);
            if true_class == est_z;
                Result_active_sta(i,k+1) = Result_active_sta(i,k+1) + 1/numsim;
            end
            det_active_sta(k+1) = mean([det(Sigma{1}), det(Sigma{2}), det(Sigma{3})]);
        end
    end
end
%}

