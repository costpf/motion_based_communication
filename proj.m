% ===================================
% FUNCTION z = proj(r, x)
% -----------
% DESCRIPTION:  Generate an actual observation taken by the receiver. The
%               Gaussian noise is added to the camera projection model.
% INPUT:    r   True relative pose of the receiver to the sender's
%               reference frame.
%           x   True trajectory of the sender in the sender's reference
%               frame.
% OUTPUT:   z   The actual observation taken by the receiver.
% DEPENDENCIES: obs(r, x)   To obtain the camera projection.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function z = proj(r, x)
% Projection (with noise)
sigma = sqrt(0.3);

z = obs(r, x);
for i = 1:length(z)
    z(i) = z(i) + normrnd(0,sigma);
end
end
