% ===================================
% SCRIPT  camera_define.m
% -----------
% DESCRIPTION:  Define the intrinsic parameters of the camera and save to
%               a mat file.
% OUTPUT:   camera_param.mat    A mat file containing camera calibration matrix.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================

% Intrinsic parameters
% 0.02x0.02 Image sensor, 100x100pixels, f = 0.06

ku = 5000; % pixel per unit length in u
kv = 5000; % pixel per unit length in v
u0 = 50;   % principal point in u
v0 = 50;   % principal point in v
f = 0.06;  % focul length
K = [ku*f, 0, u0;  % Camera calibration matrix
     0, kv*f, v0;
     0, 0, 1];
save('camera_param.mat', 'K');