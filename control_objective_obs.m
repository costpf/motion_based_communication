% ===================================
% FUNCTION J = control_objective_obs(log_phi, mu, Sigma, u, Codebook, numpoints)
% -----------
% DESCRIPTION:  Evaluate control objective for the observation entropy
%               policy.
% INPUT:    log_phi     M-by-1 vector with log of multinomial
%                       parameters.
%           mu          M-by-1 cell with 6-by-1 vectors representing the
%                       means for r.
%           Sigma       M-by-1 cell with 6-by-6 covariance matrix for r.
%           u           Control input.
%           Codebook    3-by-1 cell with numpoints-by-3 vector in each
%                       cell element.
%           numpoints   Number of points in one trajectory.
% OUTPUT:   
%           J     Calculated value for the control objective.
% DEPENDENCIES: retrieve(log_phi)   To compute phi from log_phi
%               Fderiv(mu, x)
%               Gderiv(mu, x)
%               obs(mu, x)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function J = control_objective_obs(log_phi, mu, Sigma, u, Codebook, numpoints)
% Parameters (must be the same as in ekf_update.m)
load('ekf_param.mat');

phi = retrieve(log_phi);

J = zeros(length(Codebook),1);
H = cell(length(Codebook),1);
obs_est = cell(length(Codebook),1);
for i = 1:length(Codebook);
    mu_bar = mu{i} + u;
    % Avoid singularities
    if norm(mu_bar(1:3)) > pi;
        mu_bar(1:3) = (1 - 2*pi/norm(mu_bar(1:3)))*mu_bar(1:3);
    end
    
    Sigma_bar = Sigma{i} + R;
    
    F = Fderiv(mu_bar,Codebook{i});
    G = Gderiv(mu_bar,Codebook{i});
    
    obs_est{i} = obs(mu_bar, Codebook{i});
    H{i} = S + G*Q*G' + F*Sigma_bar*F';
end
for i = 1:length(Codebook);
    %logdet_H = 2*sum(log(diag(chol(2*pi*H{i}))));
    log_N = zeros(length(Codebook),1);
    for j = 1:length(Codebook);
        log_N(j) = -0.5*2*sum(log(diag(chol(2*pi*H{j})))) - 0.5*(obs_est{i} - obs_est{j})'/H{j}*(obs_est{i} - obs_est{j});
    end
    log_sum = log_phi + log_N;
    log_sum_max = max(log_sum);
    log_sum = log_sum - repmat(log_sum_max, 3, 1);
    %J(i) = phi(i) * (0.5*logdet_H + (log_sum_max + log(sum(exp(log_sum)))));
    J(i) = -phi(i) * (log_sum_max + log(sum(exp(log_sum))));
end
J = sum(J);
if isinf(J);
    disp('J is -Inf!!');
end
if isnan(J);
    disp('J is NaN!!');
end