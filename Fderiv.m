% ===================================
% FUNCTION J = Fderiv(mu, x_traj)
% -----------
% DESCRIPTION:  Compute Jacobian matrix of the observation model w.r.t. r.
% INPUT:    mu       Mean of r.
%           x_traj   Trajectory.
% OUTPUT:   J        Jacobian matrix of the observation model w.r.t. r.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function J = Fderiv(mu,x_traj)

% Intrinsic parameters
load('camera_param.mat');
 
% Extrinsic parameters
wx = mu(1);
wy = mu(2);
wz = mu(3);
tx = mu(4);
ty = mu(5);
tz = mu(6);
t = [tx; ty; tz];

Omega = [0, -wz, wy;
         wz, 0, -wx;
         -wy, wx, 0];
R = expm(Omega);

T = [R, t];

omega = [wx; wy; wz];
d1 = cross(omega, (eye(3) - R)*[1;0;0]);
D1 = [0, -d1(3), d1(2);
      d1(3), 0, -d1(1);
      -d1(2), d1(1), 0];
d2 = cross(omega, (eye(3) - R)*[0;1;0]);
D2 = [0, -d2(3), d2(2);
      d2(3), 0, -d2(1);
      -d2(2), d2(1), 0];
d3 = cross(omega, (eye(3) - R)*[0;0;1]);
D3 = [0, -d3(3), d3(2);
      d3(3), 0, -d3(1);
      -d3(2), d3(1), 0];
DPdwx = K*[(wx*Omega + D1)/norm(omega)^2*R, zeros(3,1)];
DPdwy = K*[(wy*Omega + D2)/norm(omega)^2*R, zeros(3,1)];
DPdwz = K*[(wz*Omega + D3)/norm(omega)^2*R, zeros(3,1)];
DPdtx = K*[zeros(3,3), [1;0;0]];
DPdty = K*[zeros(3,3), [0;1;0]];
DPdtz = K*[zeros(3,3), [0;0;1]];

P = K*T;
numpoints = length(x_traj)/3;
J = zeros(2*numpoints,6);
for i = 1:numpoints
    x = x_traj(3*i-2);
    y = x_traj(3*i-1);
    z = x_traj(3*i);
    proj = P*[x_traj(3*i-2); x_traj(3*i-1); x_traj(3*i); 1];
    u = proj(1)/proj(3);
    v = proj(2)/proj(3);
    du = 1/proj(3)*[x, y, z, 1, -x*u, -y*u, -z*u, -u];
    dudwx = dot(du, [DPdwx(1,:), DPdwx(3,:)]);
    dudwy = dot(du, [DPdwy(1,:), DPdwy(3,:)]);
    dudwz = dot(du, [DPdwz(1,:), DPdwz(3,:)]);
    dudtx = dot(du, [DPdtx(1,:), DPdtx(3,:)]);
    dudty = dot(du, [DPdty(1,:), DPdty(3,:)]);
    dudtz = dot(du, [DPdtz(1,:), DPdtz(3,:)]);
    dv = 1/proj(3)*[x, y, z, 1, -x*v, -y*v, -z*v, -v];
    dvdwx = dot(dv, [DPdwx(2,:), DPdwx(3,:)]);
    dvdwy = dot(dv, [DPdwy(2,:), DPdwy(3,:)]);
    dvdwz = dot(dv, [DPdwz(2,:), DPdwz(3,:)]);
    dvdtx = dot(dv, [DPdtx(2,:), DPdtx(3,:)]);
    dvdty = dot(dv, [DPdty(2,:), DPdty(3,:)]);
    dvdtz = dot(dv, [DPdtz(2,:), DPdtz(3,:)]);
    J(2*i-1:2*i,:) = [dudwx, dudwy, dudwz, dudtx, dudty, dudtz;
                      dvdwx, dvdwy, dvdwz, dvdtx, dvdty, dvdtz];
end