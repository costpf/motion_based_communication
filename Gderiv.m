% ===================================
% FUNCTION J = Gderiv(mu, x)
% -----------
% DESCRIPTION:  Compute Jacobian matrix of the observation model w.r.t. x.
% INPUT:    mu  Mean of r.
%           x   Trajectory.
% OUTPUT:   J   Jacobian matrix of the observation model w.r.t. x.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function J = Gderiv(mu, x)

% Intrinsic parameters
load('camera_param.mat');
 
% Extrinsic parameters
wx = mu(1);
wy = mu(2);
wz = mu(3);
tx = mu(4);
ty = mu(5);
tz = mu(6);
t = [tx; ty; tz];

Omega = [0, -wz, wy;
         wz, 0, -wx;
         -wy, wx, 0];
R = expm(Omega);

T = [R, t];

P = K*T;

numpoints = length(x)/3;
J = zeros(2*numpoints, 3*numpoints);
for i = 1:numpoints
    proj = P*[x(3*i-2); x(3*i-1); x(3*i); 1];
    u = proj(1)/proj(3);
    v = proj(2)/proj(3);
    J(2*i-1:2*i,3*i-2:3*i) = [P(1,1) - P(3,1)*u, P(1,2) - P(3,2)*u, P(1,3) - P(3,3)*u;
                              P(2,1) - P(3,1)*v, P(2,2) - P(3,2)*v, P(2,3) - P(3,3)*v]./proj(3);
end
