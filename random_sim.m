% ===================================
% FUNCTION [true_r, mu, Sigma, log_phi, true_x, z, u] = random_sim(log_phi, mu, Sigma, Codebook, numpoints, true_r, true_class)
% -----------
% DESCRIPTION:  Choose u randomly and perform one step ekf update. Draw the
%               results.
% INPUT:    log_phi     M-by-1 vector with log of multinomial parameters.
%           mu          M-by-1 cell with 6-by-1 vectors representing the
%                       means for r.
%           Sigma       M-by-1 cell with 6-by-6 covariance matrix for r.
%           Codebook    3-by-1 cell with numpoints-by-3 vector in each
%                       cell element.
%           numpoints   Number of points in one trajectory.
%           true_r      True r at the current step.
%           true_class  True trajectory class.
% OUTPUT:   true_r      Updated true r
%           mu          Updated mu
%           Sigma       Updated Sigma
%           log_phi     Updated log_phi
%           true_x      True trajectory (with noise)
%           z           Observation
%           u           Control Input vector
% DEPENDENCIES:     control_random()    To choose control input.
%                   ekf_update(true_r, mu, Sigma, log_phi, u, numpoints,
%                   Codebook, true_class)   To implement EKF.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function [true_r, mu, Sigma, log_phi, true_x, z, u] = random_sim(log_phi, mu, Sigma, Codebook, numpoints, true_r, true_class)

u = control_random();
disp(u);

[true_r, mu, Sigma, log_phi, true_x, z] = ekf_update(true_r, mu, Sigma, log_phi, u, numpoints, Codebook, true_class);

clf(figure(1));
figure(1);
hold on;
set(gca,'Ydir','reverse');
true_image = transpose(reshape(transpose(z), 2, length(z)/2));
scatter(true_image(:,1),true_image(:,2), 'go', 'linewidth',2.0);
xlabel('u');
ylabel('v');
plot([0,100,100,0,0],[0,0,100,100,0],'m');
axis equal;

true_x = transpose(reshape(transpose(true_x), 3, length(true_x)/3));
clf(figure(2));
figure(2);
scatter3(true_x(:,1),true_x(:,2),true_x(:,3),'b');
wx = true_r(1);
wy = true_r(2);
wz = true_r(3);
tx = true_r(4);
ty = true_r(5);
tz = true_r(6);
Omega = [0, -wz, wy;
         wz, 0, -wx;
         -wy, wx, 0];
camerapos = transpose(-inv(expm(Omega))*[tx;ty;tz]);
upper = transpose((-inv(expm(Omega))*[tx;ty+1;tz])/norm(-expm(Omega)*[tx;ty+1;tz]));
ax = gca;
ax.CameraPosition = camerapos;
ax.CameraUpVector = upper;
xlabel('x');
ylabel('y');
zlabel('z');
axis equal;

drawnow;
disp(log_phi');
pause(0.5);
end