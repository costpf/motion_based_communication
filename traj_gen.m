% ===================================
% FUNCTION x = traj_gen(Codebook, index)
% -----------
% DESCRIPTION:  Generate an actual trajectory from the codebook performed
%               by the sender. The gaussian noise is added. 
% INPUT:    Codebook    3-by-1 cell with numpoints-by-3 vector in each
%                       cell element.
%           index       True trajectory class.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function x = traj_gen(Codebook, index)
sigma = sqrt(0.0001);
numpoints = length(Codebook{index})/3;
x = Codebook{index} + transpose(mvnrnd(zeros(3*numpoints,1),sigma^2*eye(3*numpoints)));
end