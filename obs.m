% ===================================
% FUNCTION [z, J] = obs(r, x)
% -----------
% DESCRIPTION:  Compute camera projection from the trajectory and the
%               relative pose.
% INPUT:    r   True relative pose of the receiver to the sender.
%           x   True trajectory of the sender.
% OUTPUT:   z   Projected image (observation without noise)
%           J   Jacobian matrix of the observation function (Optional).
% NOTE: Observation noise will be added in proj.m
% DEPENDENCIES: Fderiv(r, zeta)     To compute the jacobian.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function [z, J] = obs(r, x)
% z: 2m vector (observation)
% x: 2m vector (trajectory)
% r: [wx; wy; wz; tx; ty; tz]

% Number of points in the trajectory
n = length(x)/3;

% Intrinsic parameters
load('camera_param.mat');
 
% Extrinsic parameters
wx = r(1);
wy = r(2);
wz = r(3);
tx = r(4);
ty = r(5);
tz = r(6);
t = [tx; ty; tz];

Omega = [0, -wz, wy;
         wz, 0, -wx;
         -wy, wx, 0];
R = expm(Omega);

T = [R, t]; % External Parameters Matrix

% Generate camera projection (observation without noise)
z = zeros(2*n, 1);
for i = 1:n
    proj = K*T*[x(3*i-2); x(3*i-1); x(3*i); 1];
    z(2*i-1) = proj(1)/proj(3);
    z(2*i) = proj(2)/proj(3);
end

if nargout > 1   % two output arguments
   J = Fderiv(r, x);   % Jacobian of the function evaluated at r
end

