% ===================================
% FUNCTION R = rotz(deg)
% -----------
% DESCRIPTION:  Compute rotation matrix in SO(3) around current z axis with the
%               rotation angle of deg degrees.
% INPUT:    deg [deg]
% OUTPUT:   R (3-by-3 matrix in SO(3))
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function R = rotz(deg)
arg = deg*pi/180.0;
R = [cos(arg) -sin(arg) 0;
     sin(arg) cos(arg) 0;
     0 0 1];