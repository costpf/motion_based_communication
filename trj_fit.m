% ===================================
% FUNCTION [est_image, true_image] = trj_fit(r,Codebook,x,z,true_r)
% -----------
% DESCRIPTION: Plot estimated trajectory and the true image on the
%              receiver's image plane.
% INPUT:    r           Estimated relative pose.
%           Codebook    Trajectory codebook.
%           x           True trajectory.
%           z           Observation.
%           true_r      True relative pose.
% OUTPUT:   est_image   Estimated image recovered from the pose estimation.
%           true_image  True image (observation with noise).
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function [est_image,true_image] = trj_fit(r,Codebook,x,z,true_r)
est_image = cell(length(Codebook));
% Compute estimated trajectory
for i = 1:length(Codebook)
    est_image{i} = obs(r{i},Codebook{i});
    est_image{i} = transpose(reshape(transpose(est_image{i}), 2, length(est_image{i})/2));
end
true_image = transpose(reshape(transpose(z), 2, length(z)/2));

clf(figure(1));
clf(figure(2));

% Actual Observation taken by the receiver.
figure(1);
hold on;
set(gca,'Ydir','reverse');
for i = 1:length(Codebook)
    plot(est_image{i}(:,1),est_image{i}(:,2),'linewidth',2.0);
end
scatter(true_image(:,1),true_image(:,2), 'go', 'linewidth',2.0);
legend('trajectory class 1','trajectory class 2','trajectory class 3','true image');
xlabel('u');
ylabel('v');
plot([0,100,100,0,0],[0,0,100,100,0],'m');
axis equal;

% Ideal observation (without noise) in 3D space.
true_traj = transpose(reshape(transpose(x), 3, length(x)/3));
figure(2);
scatter3(true_traj(:,1),true_traj(:,2),true_traj(:,3),'b');
wx = true_r(1);
wy = true_r(2);
wz = true_r(3);
tx = true_r(4);
ty = true_r(5);
tz = true_r(6);
Omega = [0, -wz, wy;
         wz, 0, -wx;
         -wy, wx, 0];
camerapos = transpose(-inv(expm(Omega))*[tx;ty;tz]);
upper = transpose((-inv(expm(Omega))*[tx;ty+1;tz])/norm(-inv(expm(Omega))*[tx;ty+1;tz]));
ax = gca;
ax.CameraPosition = camerapos;
ax.CameraUpVector = upper;
xlabel('x');
ylabel('y');
zlabel('z');
axis equal;
drawnow;
end
