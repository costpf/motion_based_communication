% ===================================
% FUNCTION Codebook = code_gen(numpoints)
% -----------
% DESCRIPTION:  Generate a codebook with 3 different classes of
%               trajectories: a circle and 2 ellipses.
% INPUT:    numpoints   Number of points in one complete trajectory.
% OUTPUT:   Codebook    3-by-1 cell with numpoints-by-3 vector in each
%                       cell element.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function Codebook = code_gen(numpoints)
% Assume the trajectory is in xy-plane
% Codebook Generation
T = linspace(0,2*pi,numpoints);
Codebook = cell(3,1);
for i = 1:3;
    Codebook{i} = zeros(length(T),3);
end
for i = 1:length(T);
    Codebook{1}(i,:) = [-sin(T(i)), cos(T(i)), 0]; %Codebook1: Circular Trajectory
    Codebook{2}(i,:) = [-0.5*sin(T(i)),cos(T(i)), 0]; % Codebook2: Elliptic Trajectory
    Codebook{3}(i,:) = [-0.15*sin(T(i)), cos(T(i)), 0]; % Codebook3: Elliptic Trajectory 2
end
for i = 1:3;
    Codebook{i} = reshape(transpose(Codebook{i}),3*numpoints,1);
end
end

