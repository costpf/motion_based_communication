% ===================================
% FUNCTION u = control_obs(log_phi, mu, Sigma, Codebook, numpoints)
% -----------
% DESCRIPTION:  Choose u according to the observation entropy policy from
%               the control space.
% INPUT:    log_phi     M-by-1 vector with log of multinomial parameters.
%           mu          M-by-1 cell with 6-by-1 vectors representing the
%                       means for r.
%           Sigma       M-by-1 cell with 6-by-6 covariance matrix for r.
%           Codebook    3-by-1 cell with numpoints-by-3 vector in each
%                       cell element.
%           numpoints   Number of points in a trajectory.
% OUTPUT:   u   Control input
% DEPENDENCIES: control_objective_obs(log_phi, mu, Sigma, u, Codebook, numpoints)
%                                       To evaluate the control objective.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function u = control_obs(log_phi, mu, Sigma, Codebook, numpoints)
numdisc = 5; % descretize the control choices.
choice = linspace(-0.4,0.4,numdisc);

input = zeros(6, length(choice)^3);
output = zeros(length(choice)^3, 1);
l = 1;
for i = 1:numdisc;
    for j = 1:numdisc;
        for k = 1:numdisc;
        input(:,l) = [choice(i); choice(j); choice(k);0;0;0];
        l = l + 1;
        end
    end
end


for l = 1:length(input);
    output(l) = feval(@control_objective_obs, log_phi, mu, Sigma, input(:,l), Codebook, numpoints);
end
[~,minindex] = min(output);
u = input(:, minindex);
end