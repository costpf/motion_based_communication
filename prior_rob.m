% ===================================
% FUNCTION [mu, Sigma, log_phi] = prior_rob(z, Codebook)
% -----------
% DESCRIPTION:  Perform DLT and LM algorithm to initialize the belief
%               state.
% INPUT:    z           1st observation (2n vector) 
%           Codebook    3-by-1 cell with numpoints-by-3 vector in each
%                       cell element.
% OUTPUT:   mu          M-by-1 cell with 6-by-1 vectors representing the
%                       means for r.
%           Sigma       M-by-1 cell with 6-by-6 covariance matrix for r.
%           log_phi     M-by-1 vector with log of multinomial
%                       parameters.
% DEPENDENCIES: obs(r,x)    To run levenberg-marqurdt algorithm.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function [mu, Sigma, log_phi] = prior_rob(z_obs, Codebook)
% z is a 2n vector (1st observation)

mu = cell(length(Codebook),1);
Sigma = cell(length(Codebook),1);
log_phi = zeros(length(Codebook),1);

% Number of points in the trajectory
n = length(z_obs)/2;

% Intrinsic parameters
load('camera_param.mat');

numcand = 10; % Run DLT numcand times and choose the best results befor LS minimization.
for i = 1:length(Codebook);
    candidates = zeros(6,numcand);
    resdlt = zeros(1,numcand);
    for k = 1:numcand;
        % Direct Linear Transformation.
        A = zeros(20, 12);
        perm = randperm(n);
        for j = 1:10; %  Randomly choose 10 points in the trajectory.
            x = Codebook{i}(3*perm(j) - 2);
            y = Codebook{i}(3*perm(j) - 1);
            z = Codebook{i}(3*perm(j));
            u = z_obs(2*perm(j) - 1);
            v = z_obs(2*perm(j));
            A(2*j-1:2*j,:) = [x, y, z, 1, 0, 0, 0, 0, -u*x, -u*y, -u*z, -u;
                              0, 0, 0, 0, x, y, z, 1, -v*x, -v*y, -v*z, -v];
        end
        [B, D] = eig(transpose(A)*A);   % Singular value decomposition
        [~, minindex] = min(diag(D));   % Choose minimal eigne value of A.
        p_est = B(:,minindex);
        P_est = transpose(reshape(p_est,4,3));
        P_est = P_est./p_est(12);   % Projection matrix. P = TK.
        T_est = K\P_est; % = [R|t]
        T_est = T_est./norm(T_est(:,1)); % External parameters matrix
        t_est = T_est(:,4); % Translational vector
        R_est = T_est(:,1:3); % Rotational matrix (not in SO(3))
        % Post processing (Approximate R_est with SO(3))
        [U,~,V] = svd(R_est);
        R_est = U*transpose(V);
        if det(R_est) < 0;
            R_est(:,3) = cross(R_est(:,1), R_est(:,2));
        end

        % Resulting state estimate. (by DLT)
        theta_est = acos((trace(R_est) - 1)/2);
        omega_est = theta_est/(2*sin(theta_est)) * [R_est(3,2) - R_est(2,3); R_est(1,3) - R_est(3,1); R_est(2,1) - R_est(1,2)];
        r_est = [omega_est(1); omega_est(2); omega_est(3); t_est];

        % Correct omega to avoid singularities.
        if norm(r_est(1:3)) > pi;
            r_est(1:3) = (1 - 2*pi/norm(r_est(1:3)))*r_est(1:3);
        end
        candidates(:,k) = r_est;
        resdlt(k) = norm(feval(@obs,r_est,Codebook{i}) - z_obs);
    end
    [~,mindlt] = min(resdlt);
    r_est = candidates(:,mindlt);
    
    % Levenberg-Marquardt Algorithm for LSM
    lb = [];
    ub = [];
    options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt','Display','iter','Jacobian','on','MaxIter',200,'TolFun',1e-7,'TolX',1e-7);
    [r_est,resnorm,~,~,~] = lsqcurvefit(@obs,r_est,Codebook{i},z_obs,lb,ub,options);
    mu{i} = r_est;
    Sigma{i} = 10000*eye(6); % Initial covariance defined.
    log_phi(i) = -0.5*resnorm/10000;
end


        


