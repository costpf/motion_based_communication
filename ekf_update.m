% ===================================
% FUNCTION [true_r, mu, Sigma, log_phi, true_x, z] = ekf_update(true_r, mu, Sigma, log_phi, u, numpoints, Codebook, true_class)
% -----------
% DESCRIPTION:  Implement EKF algorithm and update the belief state.
% INPUT:    true_r      True relative pose at the current step.
%           mu          M-by-1 cell with 6-by-1 vectors representing the
%                       means for r.
%           Sigma       M-by-1 cell with 6-by-6 covariance matrix for r.
%           log_phi     M-by-1 vector with log of multinomial
%                       parameters.
%           u           Control input vector.
%           numpoints   Number of points in one trajectory.
%           Codebook    3-by-1 cell with numpoints-by-3 vector in each
%                       cell element.
%           true_class  True trajectory class.
% OUTPUT:   true_r      Updated true r.
%           mu          Updated mu.
%           Sigma       Updated Sigma.
%           log_phi     Updated log_phi
%           true_x      True trajectory (with noise).
%           z           Observation.
% DEPENDENCIES: traj_gen(Codebook, true_class) To generate a trajectory.
%               proj(true_r, true_x)           To take an observation.
%               Fderiv(mu, x)    Jacobian w.r.t. r
%               Gderiv(mu, x)    Jacobian w.r.t. x
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
% ===================================
function [true_r, mu, Sigma, log_phi, true_x, z] = ekf_update(true_r, mu, Sigma, log_phi, u, numpoints, Codebook, true_class)
% Parameters
Q = 0.0001*eye(3*numpoints); % Trajectory noise (3n * 3n)
R = 0.00001*eye(6); % Control noise (6 * 6);
S = 0.3*eye(2*numpoints); % Observation noise (2n * 2n)
save('ekf_param.mat', 'Q','R','S');

% True control input (with noise)
true_u = u + transpose(mvnrnd([0;0;0;0;0;0],R));
% True state (with noise)
true_r = true_r + true_u;
%  Avoid singularities
if norm(true_r(1:3)) > pi;
        true_r(1:3) = (1 - 2*pi/norm(true_r(1:3)))*true_r(1:3);
end
% New trajectory
true_x = traj_gen(Codebook, true_class);
% New observation
z = proj(true_r, true_x);

% EKF algorithm
for i = 1:length(log_phi)
    mu_bar = mu{i} + u;
    % Avoid singularities
    if norm(mu_bar(1:3)) > pi;
        mu_bar(1:3) = (1 - 2*pi/norm(mu_bar(1:3)))*mu_bar(1:3);
    end
    
    Sigma_bar = Sigma{i} + R;
    
    F = Fderiv(mu_bar,Codebook{i});
    G = Gderiv(mu_bar,Codebook{i});
    
    H = S + G*Q*G' + F*Sigma_bar*F';
    K = Sigma_bar*F'/H;
    
    innovation = z - feval(@obs,mu_bar,Codebook{i});
    mu{i} = mu_bar + K*innovation;
    % Avoid singularities
    if norm(mu{i}(1:3)) > pi;
        mu{i}(1:3) = (1 - 2*pi/norm(mu{i}(1:3)))*mu{i}(1:3);
    end
    Sigma{i} = (eye(6) - K*F)*Sigma_bar;
    
    %compute updated log_phi
    logdet_H = 2*sum(log(diag(chol(H))));
    phi_update_log = -0.5*logdet_H - 0.5*innovation'/H*innovation;
    log_phi(i) = phi_update_log + log_phi(i);
end
log_phi = log_phi - repmat(max(log_phi),3,1);
end


